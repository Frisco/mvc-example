<?php

/**
 * Class User_Model
 */
class User_Model extends Model
{
    /**
     * User_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function userList()
    {
        return $this->db->select("users");
    }

    /**
     * @param $data
     */
    public function create($data){
        $this->db->insert("users", array(
            "login" => $data['login'],
            "password" => Hash::create(Config::get("hashing/algorithm") , $data['password'], Config::get("hashing/passwordSalt")),
            "role" => $data['role']
        ));
    }

    /**
     * @param $id
     * @return bool|int
     */
    public function delete($id){
        $data  = $this->db->select("users", array("id" => $id));
        if($data[0]['role'] == "owner"){
            return false;
        }
        return $this->db->delete("users", "`id` = ".$id);
    }

    /**
     * @param $id
     * @return array
     */
    public function listUserById($id){
        return $this->db->select("users", array("id" => $id));
    }

    /**
     * @param $data
     */
    public function editSave($data){
        $this->db->update("users", array(
            "login" => $data['login'],
            "password" => Hash::create(Config::get("hashing/algorithm") , $data['password'], Config::get("hashing/passwordSalt")),
            "role" => $data['role']
        ), "`id` = ".$data['id']);
    }
}