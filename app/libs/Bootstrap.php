<?php

/**
 * Class Bootstrap
 */
class Bootstrap
{

    private $_url               = null;
    private $_controller        = null;

    /**
     * @return bool
     */
    public function init()
    {
        $this->_getUrl();

        if(empty($this->_url[0])){
            $this->_loadDefaultController();
            return false;
        }

        $this->_loadExistingController();
        $this->_callControllerMethod();
        return $this;
    }

    /**
     *
     */
    private function _getUrl()
    {
        $url_parts = explode('?', $_SERVER['REQUEST_URI']);
        $url = trim($url_parts[0], "/");
        $url = rtrim($url, "/");
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode("/", $url);
    }


    /**
     *
     */
    private function _loadDefaultController()
    {
        require Config::get("paths/controllers") . Config::get("controllers/defaultController");
        $method = Config::get("controllers/defaultMethod");
        $this->_controller = new $method();
        $this->_controller->index();
    }

    /**
     * @return bool
     */
    private function _loadExistingController()
    {
        $file = Config::get("paths/controllers") . $this->_url[0] . ".php";
        if(file_exists($file)){
            require $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0], Config::get("paths/models"));
        } else {
            $this->_error("Controller Not Found");
            return false;
        }

    }

    /**
     *
     */
    private function _callControllerMethod()
    {
        if(count($this->_url) >= 2) {
            if(!method_exists($this->_controller, $this->_url[1])) {
                $this->_error("Method Not Found");
            }

            $params = $this->_url;
            unset($params[0]);
            unset($params[1]);

            call_user_func_array(array($this->_controller, $this->_url[1]), $params);
        } else {
            $this->_controller->index();
        }
    }

    /**
     * @param $error
     */
    private function _error($error)
    {
        require Config::get("paths/controllers") . Config::get("paths/errorFile");
        $this->_controller = new Error();
        $this->_controller->index($error);
        exit;
    }
 }