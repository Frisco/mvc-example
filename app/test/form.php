<?


require "../libs/Form.php";
require "../libs/Token.php";
require "../libs/Session.php";
require "../Config.php";
require "../../config/config.php";

Session::init();

var_dump(Token::check(Form::get(Config::get("csrf/tokenName"))));

if(Form::exists()) {
    try {
        $form = new Form();

        $form   ->post('name')
            ->validate('minLength', 2)
            ->validate('maxLength', 8)

            ->post('age')
            ->validate('digit')
            ->validate('required')

            ->post('gender')

            ->post('password')
            ->validate('match', 'password_again')

            ->post('password_again')
            ->validate('required')

        ;
        $form   ->submit();

        echo 'The Form Passed';

        $data = $form->fetch();

        echo ' Do some stuff with the validated stuff back';

        echo "<pre>";
        print_r($data);
        echo "</pre>";

    } catch (Exception $e) {
        print_r(json_decode($e->getMessage()));
    }
}

?>

    <form method = "post">
        Name <input type="text" name="name" value="<?php echo Form::get('name'); ?>"/>

        Age <input type="text" name="age" value="<?php echo Form::get('age'); ?>"/>
        Gender <select name="gender">
            <option value="m" <?php echo Form::get('gender')=="m"?"selected":""; ?>>Male</option>
            <option value="f" <?php echo Form::get('gender')=="f"?"selected":""; ?>>female</option>
        </select>
        <br /><br /><br />
        Password <input type="text" name="password" value="<?php echo Form::get('password'); ?>"/><br />
        Password Again <input type="text" name="password_again" value="<?php echo Form::get('password_again'); ?>"/>
        <input type="hidden" name="<?php echo Config::get("csrf/tokenName"); ?>" value="<?php echo Token::generate(); ?>" />
        <input type="submit" />
    </form>


<?