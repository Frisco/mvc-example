<div id="content">
<form class="userForm" method="post" action="<?php echo Config::get("paths/url"); ?>user/<?php echo $this->action; ?>/<?php echo $this->user['id']; ?>">
    <label>Login</label><input type="text" name="login" value="<?php echo $this->user['login']; ?>"/><br />
    <label>Password</label><input type="password" name="password"/><br />
    <label>Role</label>
    <select name="role">
        <option value="default" <?php echo $this->user['role']=='default'?"selected":""; ?>>Default</option>
        <option value="admin" <?php echo $this->user['role']=='admin'?"selected":""; ?>>Admin</option>
        <option value="owner" <?php echo $this->user['role']=='owner'?"selected":""; ?>>Owner</option>
    </select><br />
    <label>&nbsp;</label><button><?php echo $this->buttonAction; ?></button>
</form>
</div>