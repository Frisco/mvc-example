<?php
class Validator
{
    public function minLength($data, $currItem, $arg)
    {
        if(strlen($data) < $arg){
            return "your string needs to be more than ".$arg." in length (".$currItem.")";
        }
    }

    public function maxLength($data, $currItem, $arg)
    {
        if(strlen($data) > $arg){
            return "your string needs to be less than ".$arg." in length (".$currItem.")";
        }
    }

    public function digit($data, $currItem)
    {
        if(ctype_digit($data) == false){
            return "your string needs to be an integer (".$currItem.")";
        }
    }

    public function email($data, $currItem)
    {
        //TODO Email Validation
    }

    public function unique($data, $currItem, $table, $field)
    {
        //TODO Unique check against table/field
    }

    public function match($data, $currItem, $field)
    {
        if(Form::get($field) != $data) {
            return "your string needs to match with ". $field. " (".$currItem.")";
        }
    }

    public function required($data, $currItem)
    {
        if(!$data){
            return "your string is required (".$currItem.")";
        }
    }

    public function __call($name, $arguments)
    {
        throw new Exception($name . " Does not exist inside: " . __CLASS__);
    }

}