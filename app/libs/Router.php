<?php

/**
 * Class Router
 */
class Router {
    /**
     * @var array
     */
    private $_routes = [];

    /**
     * Router constructor.
     */
    public function __construct(){

    }

    /**
     * @param $uri
     * @param $route
     */
    public function add($uri, $route)
    {
        $this->_routes[$uri] = $route;
    }

    /**
     *
     */
    public function submit()
    {
        $url_parts = explode('?', $_SERVER['REQUEST_URI']);
        $url = trim($url_parts[0], "/");
        $url = rtrim($url, "/");
        $url = filter_var($url, FILTER_SANITIZE_URL);

        echo("<hr>");
        foreach($this->_routes as $uriKey => $uriValue){
            echo $uriKey . " XXXXXXX " . var_dump($uriValue) . "<br>";
        }
        echo("<hr>");

         echo "--" .$url ."--";
    }
}