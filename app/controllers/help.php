<?php

/**
 * Class Help
 */
class Help extends Controller
{
    /**
     * Help constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view->heading = "Help";
        $this->view->title = "Admin | Help";
    }

    /**
     *
     */
    public function index(){
        $this->view->render("header");
        $this->view->render("help/index");
        $this->view->render("footer");
    }
}