<?php

/**
 * Class Login_Model
 */
class Login_Model extends Model
{
    /**
     * Login_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function loginDo()
    {
        $data = $this->db->select("users", array(
            "login" => $_POST['login'],
            "password" => Hash::create(Config::get("hashing/algorithm") , $_POST['password'], Config::get("hashing/passwordSalt"))
        ));

        if(count($data) == 1){
            Session::set("loggedIn", true);
            Session::set("role", $data[0]['role']);
            Session::set("userId", $data[0]['id']);

            header("location: ".Config::get("paths/url")."dashboard");
        } else {
            header("location: ".Config::get("paths/url")."login");
        }

    }
      
 }