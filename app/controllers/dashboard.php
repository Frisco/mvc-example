<?php

/**
 * Class Dashboard
 */
class Dashboard extends Controller
{
    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
        Auth::handleAuth();

        $this->view->heading = "Dashboard";
        $this->view->title = "Admin | Dashboard";
        $this->view->js = array('dashboard/js/default.js');
    }

    /**
     *
     */
    public function index(){
        $this->view->render("header");
        $this->view->render("dashboard/index");
        $this->view->render("footer");
    }

    /**
     *
     */
    public function logout()
    {
        Session::destroy();
        header("location: ".Config::get("paths/url")."login");
        exit;
    }

    /**
     *
     */
    public function xhrInsertListing()
    {
        $this->model->xhrInsertListing();
    }

    /**
     *
     */
    public function xhrGetListings()
    {
        $this->model->xhrGetListings();
    }

    /**
     *
     */
    public function xhrDeleteListing()
    {
        $this->model->xhrDeleteListing();
    }
}