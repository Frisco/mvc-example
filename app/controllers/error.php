<?php

/**
 * Class Error
 */
class Error extends Controller
{
    /**
     * Error constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view->heading = "Error";
        $this->view->title = "Admin | Error";
    }

    /**
     * @param $message
     */
    public function index($message){
        $this->view->message = $message;
        $this->view->render("header");
        $this->view->render("error/index");
        $this->view->render("footer");
    }
      
 }