<?php

/**
 * Class User
 */
class User extends Controller
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        Auth::handleAuth();
        /*
        $logged = Session::get("loggedIn");
        $role = Session::get("role");

        if($logged == false || $role != "owner"){
            Session::destroy();
            header("location: ".URL."login");
            exit;
        }
        */
    }

    /**
     *
     */
    public function index(){
        $this->view->heading = "Users";
        $this->view->title = "Admin | Users";
        $this->view->userList = $this->model->userList();

        $this->view->render("header");
        $this->view->render("user/index");
        $this->view->render("footer");
    }
    /**
     * @param $id
     */
    public function add()
    {
        $this->view->heading = "Users - Add";
        $this->view->title = "Admin | Users - Add";
        $this->view->action = "create";
        $this->view->buttonAction = "Add";

        $this->view->render("header");
        $this->view->render("user/form");
        $this->view->render("footer");
    }
    /**
     *
     */
    public function create(){
        $data['login'] = $_POST['login'];
        $data['password'] = $_POST['password'];
        $data['role'] = $_POST['role'];

        //TODO error checking

        $this->model->create($data);
        header("location: " . Config::get("paths/url") . "user");
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $this->view->heading = "Users - Edit";
        $this->view->title = "Admin | Users - Edit";
        $this->view->action = "editSave";
        $this->view->buttonAction = "Update";
        $this->view->user = $this->model->listUserById($id)[0];

        $this->view->render("header");
        $this->view->render("user/form");
        $this->view->render("footer");
    }

    /**
     * @param $id
     */
    public function editSave($id)
    {
        $data['login'] = $_POST['login'];
        $data['password'] = $_POST['password'];
        $data['role'] = $_POST['role'];
        $data['id'] = $id;

        //TODO error checking

        $this->model->editSave($data, $id);
        header("location: " . Config::get("paths/url") . "user");
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->model->delete($id);
        header("location: " . Config::get("paths/url") . "user");
    }
}