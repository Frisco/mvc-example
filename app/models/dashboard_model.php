<?php

/**
 * Class Dashboard_Model
 */
class Dashboard_Model extends Model
{
    /**
     * Dashboard_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function xhrInsertListing()
    {
        $str_json = file_get_contents('php://input');
        $dataIn = json_decode($str_json);
        $this->db->insert("data", array("text" => $dataIn->text));
        $data = array("text" => $dataIn->text, "id" => $this->db->lastInsertId());
        echo json_encode($data);
    }

    /**
     *
     */
    public function xhrGetListings(){
        echo json_encode($this->db->select("data"));
    }

    /**
     *
     */
    public function xhrDeleteListing(){
        $str_json = file_get_contents('php://input');
        $dataIn = json_decode($str_json);
        $data = $this->db->delete("data", "`id`=".$dataIn->id);
        $data = array("success" => $data, "id" => $dataIn->id);
        echo json_encode($data);

    }
}