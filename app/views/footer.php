<div id="footer">
    &copy; <?php echo date("Y"); ?> <?php echo Config::get("general/company"); ?>
    <div style="float:right;">
        Render Time: <?php echo View::renderTime(); ?> |
        Version: <?php echo Config::get("general/version"); ?>
    </div>
</div>
</div>
</body>
</html>