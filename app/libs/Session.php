<?php

/**
 * Class Session
 */
class Session {
    /**
     * Session constructor.
     */
    function __construct()
    {
        //Todo - Constructor Class
    }

    /**
     *
     */
    public static function init()
    {
        session_start();
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        session_start();
        return $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        if(isset($_SESSION[$key]))
        return $_SESSION[$key];
    }

    /**
     *
     */
    public static function destroy()
    {
        unset($_SESSION);
        session_destroy();
    }

    public static function delete($name)
    {
        if(self::exists($name)){
            unset($_SESSION[$name]);
        }
    }

    public static function exists($name)
    {
        return (isset($_SESSION[$name])) ? true : false;
    }
 }