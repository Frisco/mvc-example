<?php

/**
 * Class Index
 */
class Index extends Controller
{
    /**
     * Index constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view->heading = "Welcome";
        $this->view->title = "Admin";
    }

    /**
     *
     */
    public function index(){
        $this->view->render("header");
        $this->view->render("index/index");
        $this->view->render("footer");
    }

}