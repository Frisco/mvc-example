docReady(function(){

    xhr.sendXHR({}, "POST", "dashboard/xhrGetListings", function(obj)
    {
        for(var i = 0; i < obj.length; i++)
        {
            document.querySelector("#listings").innerHTML += "<div>" + obj[i].text + "<a href='#' rel='" + obj[i].id + "' class='del button'>X</a></div>";
        }
        addDeleteListeners();
    });

    document.querySelector("#contentForm").addEventListener("submit", function()
    {
        xhr.sendXHR(xhr.serializeForm(this), "POST", this.action, function(obj)
        {
            document.querySelector("#listings").innerHTML += "<div>" + obj.text + "<a href='#' rel='" + obj.id + "' class='del button'>X</a></div>";
            addDeleteListeners();
        });
        event.preventDefault();
        return false;
    }, false);
});

function addDeleteListeners()
{
    [].forEach.call(document.querySelectorAll('.del'), function(elm)
    {
        elm.addEventListener('mousedown', function()
        {
            var data = {"id" : this.rel};
            delItem = elm;
            xhr.sendXHR(data, "POST", "dashboard/xhrDeleteListing", function(obj)
            {
                if(obj.success==1){
                    elm.parentNode.parentNode.removeChild(elm.parentNode);
                }
            });
        }, false);
    });
}